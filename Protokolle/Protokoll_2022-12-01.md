# Pikos Python Kurs am 17.08.2022

Präsentation:  
https://md.ha.si/s/h9wZTQe7G#

## Links
Haecksen:  
https://www.haecksen.org

Wau-Holland-Stiftung:  
https://www.wauland.de/de/

Einfache Videokonferenzen:  
https://senfcall.de/

***
Fragen: Piko@riseup.net
Aufgaben und Protokolle:
https://gitlab.com/sudo_piko/pythonkurs-c

Gruppen:  
https://md.ha.si/8yhOjMR0SymS3MPEkInSQg#



## Pikos Kommandozeile
```
>>> print("Hallo Welt!")
Hallo Welt!
>>> import turtle
>>> import turtl
Traceback (most recent call last):
  File "<pyshell>", line 1, in <module>
ModuleNotFoundError: No module named 'turtl'
>>> 3+8
11
>>> 3*8
24
>>> 9/3
3.0
>>> 10/3
3.3333333333333335
>>> 10 % 3
1
```
%-Operator: "modulo" = teilen mit Rest
```
>>> from turtle import *
>>> forward(100)
>>> right(90)
```